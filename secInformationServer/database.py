#from dbIO import Encoder, Decoder, loads, dumps, DBEntry, isoformat
from sortedcontainers import SortedList
from functools import reduce
import Pyro4
from secInformationServer.entry import Entry, isoformat, datetime
from secInformationServer.dbIO import loads, dumps, Decoder, Encoder
from fnmatch import fnmatch
from os import getcwd
from glob import glob

DEBUG = False

def DEBUGOUT(*args):
	if DEBUG:
		print(*args)

class Database:
	def __init__(self, configFile: str="config.json"):
		self.keys = dict()
		self.tags = dict()
		self.dates = SortedList()

		with open(configFile, "r") as configFile:
			self.config = loads(configFile.read(), cls=Decoder)
			configFile.close()

	def addEntry(self, **kwargs):
		entry = Entry(**kwargs)

		self.keys[entry["key"]] = entry
		self.dates.add(entry)
		for tag in entry["tags"]:
			if tag not in self.tags:
				self.tags[tag] = set()
			self.tags[tag].add(entry)

	def removeEntry(self, key):
		entry = self.keys[key]
		del self.keys[key]
		for tag in entry["tags"]:
			self.tags[tag].remove(entry)

		self.dates.remove(entry)

	def search(self, **kwargs):
		searchingMethods = dict()
		searchingMethods["tags"] = self.searchTags
		searchingMethods["dates"] = self.searchDateRange
		searchingMethods["pattern"] = self.searchKeys

		results = list()
		for dimension in kwargs:
			DEBUGOUT(dimension, "\t", searchingMethods[dimension](kwargs[dimension]))
			results.append(set(searchingMethods[dimension](kwargs[dimension])))

		result = [e.toDict() for e in reduce(set.intersection, results)]
		return result

	def searchTags(self, tags, **kwargs):
		DEBUGOUT("tags:", tags)
		temp = [self.tags[tag] for tag in tags]
		return [e for e in reduce(set.intersection, temp)]

	def searchDateRange(self, dates, **kwargs):
		DEBUGOUT("dates:", dates)
		if len(dates) == 1:
			start = datetime.strptime(dates[0], isoformat)
			end = datetime.strptime(dates[0], isoformat)
		else:
			start = datetime.strptime(dates[0], isoformat)
			end = datetime.strptime(dates[1], isoformat)
		results = list(self.dates.irange(minimum=start, maximum=end))
		return results

	def searchKeys(self, pattern: str, **kwargs):
		DEBUGOUT("pattern:", pattern)
		return [self.keys[key] for key in self.keys if fnmatch(key, pattern)]

	def __getitem__(self, key):
		return self.keys[key]

		