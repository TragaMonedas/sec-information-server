import Pyro4
from datetime import MINYEAR, date, timedelta, datetime

isoformat = r'%Y-%m-%d'#,%H:%M:%S'

default = {
	"key": "default",
	"date": str(date.today()),
	"tags": []
}

class Entry(dict):

	def __init__(
		self,
		key: str=default["key"],
		date: str=default["date"],
		tags: list=default["tags"],
		**kwargs):

		self.attributes = ["key", "date", "tags"]
		self["key"] = key
		self["date"] = date
		self["tags"] = tags

		self.update(**kwargs)
		self.attributes += [e for e in kwargs.keys() if e not in self.attributes]

	def __lt__(self, other):
		if type(other) in (str,):
			if self["date"]:
				return datetime.strptime(self["date"], isoformat) < datetime.strptime(other, isoformat)
		elif self["date"] and type(other) in (date, datetime):
			return datetime.strptime(self["date"], isoformat) < other
		else:
			print("one entry has no date or other is not comparable to date(time)")

	def __le__(self, other):
		if type(other) in (str,):
			if self["date"]:
				return datetime.strptime(self["date"], isoformat) <= datetime.strptime(other, isoformat)
		elif self["date"] and type(other) in (date, datetime):
			return datetime.strptime(self["date"], isoformat) <= other
		else:
			print("one entry has no date or other is not comparable to date(time)")
			
	def __gt__(self, other):
		if type(other) in (str,):
			if self["date"]:
				return datetime.strptime(self["date"], isoformat) > datetime.strptime(other, isoformat)
		elif self["date"] and type(other) in (date, datetime):
			return datetime.strptime(self["date"], isoformat) > other
		else:
			print("one entry has no date or other is not comparable to date(time)")
			
	def __ge__(self, other):
		if type(other) in (str,):
			if self["date"]:
				return datetime.strptime(self["date"], isoformat) >= datetime.strptime(other, isoformat)
		elif self["date"] and type(other) in (date, datetime):
			return datetime.strptime(self["date"], isoformat) >= other
		else:
			print("one entry has no date or other is not comparable to date(time)")

	def __str__(self):
		temp = ""
		for attribute in self.attributes:
			temp += (attribute + ": " + str(self[attribute]) + "\n")
		return temp

	def __repr(self):
		return "Entry " + self["key"]

	def __hash__(self):
		return hash(self["key"])

	def toDict(self):
		return dict(self)
