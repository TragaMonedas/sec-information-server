from json import JSONEncoder, dumps, JSONDecoder, loads
from secInformationServer.entry import Entry, isoformat
from datetime import date

class Encoder(JSONEncoder):
	def default(self, o):
		if type(o) is Entry:
			return {"__type__": "Entry", "kwargs": o}
		if type(o) is date:
			return {"__type__": "date", "args": [o.strftime]}
		return super(Encoder, self).default(o)


class Decoder(JSONDecoder):
	symTable = dict()
	symTable["DBEntry"] = Entry
	symTable["date"] = lambda d: date(d)

	def __init__(self, *args, **kwargs):

		def decode(dct: dict):
			if "__type__" in dct:
				if "args" not in dct and "kwargs" not in dct:
					return self.symTable[dct["__type__"]]()
				if "args" in dct and "kwargs" not in dct:
					return self.symTable[dct["__type__"]](dct["args"])
				if "args" in dct and "kwargs" in dct:
					return self.symTable[dct["__type__"]](*dct["args"], **dct["kwargs"])
				if "args" not in dct and "kwargs" in dct:
					return self.symTable[dct["__type__"]](**dct["kwargs"])
			else:
				return dct

		kwargs.update({"object_hook": decode})
		super(Decoder, self).__init__(*args, **kwargs)
