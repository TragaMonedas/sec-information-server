from time import sleep
from datetime import datetime
from secInformationServer.dbIO import Encoder, dumps
import Pyro4


print("starting backup")

while True:	
	with open("service", mode="r") as file:
		serviceURI = file.readline()
		file.close()
	with Pyro4.Proxy(serviceURI) as service:
		print("saving database", datetime.now())
		with open(service.getConfig()["backupFile"], "w") as backupFile:
			backupFile.write(dumps(service.getDatabase(), indent=4, sort_keys=True, cls=Encoder))
			backupFile.close()
	sleep(60)