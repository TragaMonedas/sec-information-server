import sys
import Pyro4
from datetime import date, datetime
from secInformationServer.entry import Entry
from pprint import pprint

file = open("../service", mode="r")
serviceURI = file.readline()
file.close()

service = Pyro4.Proxy(serviceURI)

#addEntry = service.addEntry
def addEntry(**kwargs):
	service.addEntry(**kwargs)

def search(**kwargs):
	return [Entry(**e) for e in service.search(**kwargs)]

def searchKeys(pattern):
	return service.searchKeys(pattern)

def searchTags(tags, **kwargs):
	return service.searchTags(tags, **kwargs)

def searchDateRange(dates, **kwargs):
	return service.searchDateRange(dates, **kwargs)

listEntries = service.listEntries
removeEntry = service.removeEntry
