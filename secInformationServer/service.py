from __future__ import print_function
import Pyro4
from database import Database
from secInformationServer.entry import Entry
from secInformationServer.dbIO import Decoder, loads
from pprint import pprint
from os import getcwd


database = Database()

@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class Service:
	def __init__(self):
		print("starting service from directory", getcwd())

	@Pyro4.expose
	def search(self, **kwargs):
		print("searching with parameters:", *list(kwargs.keys()))
		return database.search(**kwargs)

	@Pyro4.expose
	def searchTags(self, tags, **kwargs):
		print("searching for tags", *tags)
		return database.searchTags(tags, **kwargs)

	@Pyro4.expose
	def searchDateRange(self, dates, **kwargs):
		print("searching for dates", dates)
		return database.searchDateRange(dates, **kwargs)

	@Pyro4.expose
	def searchKeys(self, pattern, **kwargs):
		print("searching for keys matching", pattern)
		return database.searchKeys(pattern, **kwargs)

	@Pyro4.expose
	def addEntry(self, **kwargs):
		print("adding entry", kwargs.get("key", "default name"))
		if kwargs["key"] in self.listEntries():
			print("key", kwargs["key"], "already in database")
			return
		database.addEntry(**kwargs)

	@Pyro4.expose
	def removeEntry(self, key):
		print("removing entry", key)
		database.removeEntry(key)

	@Pyro4.expose
	def listEntries(self):
		return database.keys.keys()

	@Pyro4.expose
	def get(self, key):
		if key in database.keys:
			return database.keys[key].toDict()
		else:
			print(key, "not in database")

	@Pyro4.expose
	def __getitem__(self, key):
		return database[key]

	@Pyro4.expose
	def getDatabase(self):
		return database.keys

	@Pyro4.expose
	def getConfig(self):
		return database.config


def main():
	with Pyro4.Daemon() as daemon:
		service = Service()
		serviceURI = daemon.register(service)

		file = open("service", mode="w")
		file.write(str(serviceURI) + "\n")
		file.close()

		backupFile = database.config["backupFile"]

		with open(backupFile, mode="r") as file:
			temp = loads(file.read(), cls=Decoder)
			for entry in temp:
				service.addEntry(**temp[entry])
		daemon.requestLoop()



if __name__ == "__main__":
	main()
