from secInformationServer.client import search, searchTags, searchKeys, searchDateRange, addEntry, listEntries, removeEntry

addEntry(key="bla", date="2019-4-4", tags=["1", "2"])
#removeEntry("bla")
print(search(tags=["1"]))
print(search(dates=["2019-4-4"]))
print(search(dates=["2019-4-4", "2019-4-5"]))
print(search(pattern="b*a"))
print(search(pattern="b*a", dates=["2019-4-4", "2019-4-5"], tags=["1"]))


