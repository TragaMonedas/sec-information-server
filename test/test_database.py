from secInformationServer.database import Database, Decoder, loads

database = Database("../config.json")
with open("../backup/backup.json", mode="r") as file:
	temp = loads(file.read(), cls=Decoder)
	for entry in temp:
		print(temp)
		database.addEntry(**temp[entry])


database.addEntry()
database.addEntry(key="blabla", tags=["1", "2"], date="2019-4-5")
print("testing searchTags")
print(database.searchTags(["1"]))
print(database.searchTags(["1", "2"]))
print("testing searchDateRange")
print(database.searchDateRange(["2019-4-15"]))
print(database.searchDateRange(["2019-4-4", "2019-4-17"]))
print("testing searchKeys")
print(database.searchKeys("b*abla"))
print("testing search")
print(database.search(tags=["1"], dates=["2019-4-5", "2019-4-15"], pattern="*"))